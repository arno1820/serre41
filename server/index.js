const express = require('express');
const bodyParser = require('body-parser');
const dgram = require('dgram');
const fs = require('fs');
const PORT = process.env.PORT || 8080;
const path = require('path');
const app = express();


let data = [];
let previousSave = new Date().getTime();
const saveInterval = 600000 // Interval when to save the data
const socket = dgram.createSocket('udp4');
socket.bind(2154);
socket.addListener("message", (msg, rinfo) => {
    const currentdate = new Date();
    const datetime = currentdate.getTime();
    let message = msg.toString();
    message = message.split("T")[1].split("H");
    const temp = Number(message[0]);
    message = message[1].split("P");
    const humi = Number(message[0]);
    const pressure = Number(message[1]);
    data.push([datetime, temp, humi, pressure]);
    //console.log('message was received', datetime + msg.toString());
    if(datetime - previousSave > saveInterval && data.length !== 0){ //TODO sla data op elke 10 minuten en maar 1 data punt
        previousSave = currentdate.getTime();
        // Calculate average
        const sum = data.reduce((acc, c) => {return [acc[0]+c[0], acc[1]+c[1],acc[2]+c[2],acc[3]+c[3]]}, [0,0,0,0]);
        const average = [
            (sum[0]/data.length).toFixed(2),
            (sum[1]/data.length).toFixed(2),
            (sum[2]/data.length).toFixed(2),
            (sum[3]/data.length).toFixed(2)]
        fs.appendFile('data.txt', data[data.length-1] + '\n',  (err) => {
            if (err) throw err;
        });
        data = data.slice(data.length-2, data.length);
    }
})

app.use(bodyParser.json());
app.use(express.static(`${__dirname}/../react-client/dist`));
app.get('/', (req, res) => {
    res.sendFile(path.resolve(`${__dirname}/../react-client/dist/index.html`));
});

app.get('/currentdata', (req, res) => {
    if(!data || data.length === 0){
        res.status(500).send([]);
        return;
    }
    const packet = data[data.length-1].toString();
    res.send(packet);
});

app.get('/data', (req, res) => {
    let hist = []
    fs.readFile('data.txt', 'utf8', (err, d) => {
        if (err){
	    console.log("could not send datahistory", err.toString());
	    res.status(500).send([]);
	}
        hist = d.split("\n");

        hist = hist.slice(0, hist.length-1);
        res.send(hist);
    });

});

app.listen(PORT, () => {
    console.log(`listening on port ${PORT}!`);
});
