import React, { Component } from 'react';
import Chart  from 'react-apexcharts';

export default class LineChart extends React.Component {

    constructor(props) {
        super(props);

        const data =

        this.state = {
            options: {
                chart: {
                    id: props.name,
                    group: 'data'
                },
                tooltip:{
                    x: {
                        format: "ddd dd MMM HH:mm"
                    }
                },
                stroke: {
                    curve: 'smooth'
                },
                yaxis: {
                    labels: {
                        show: true,
                        formatter: (value) => value.toFixed(2) + " " + props.unitType
                    }
                },
                xaxis: {
                    type: 'datetime',
                },
                animations: {
                    enabled: false,
                },
                colors: [props.color],
                title: {
                    text: props.name,
                    align: 'left',
                    margin: 10,
                    offsetX: 0,
                    offsetY: 0,
                    floating: false,
                    style: {
                        fontSize:  '16px',
                        color:  '#263238'
                    },
                },
                grid: {
                    show: true,
                    borderColor: '#90A4AE',
                    strokeDashArray: 0,
                    position: 'back',
                    xaxis: {
                        lines: {
                        show: true
                    }
                    },
                    yaxis: {
                        lines: {
                        show: true
                    }
                    },
                    padding: {
                        top: 0,
                        right: 0,
                        bottom: 0,
                        left: 0
                    },
                }
            },
            series: [
                {
                    name: props.name,
                    data: this.formatData(props.data, props.times)
                }
            ],
        };
    }

    componentDidUpdate(prevProps) {
        if(prevProps.data == this.props.data && prevProps.name == this.props.name)return;

        this.setState({series: [
                {
                    name: this.props.name,
                    data: this.formatData(this.props.data, this.props.times)
                }
            ]})
    }

    formatData(data, times){
        const result = [];
        data.forEach((value, index) => {
            result.push([times[index], value]);
        });
        return result;
    }

    render() {

        return (
            <div>
                <Chart
                    options={this.state.options}
                    series={this.state.series}
                    type="line"
                    height="250"
                    width="500"
                />
            </div>);
    }
}