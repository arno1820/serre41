import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link} from 'react-router-dom';
import { browserHistory } from 'react-router';
import HomePage from './HomePage';
import NavBar from './NavBar';
import axios from 'axios';
import DataChart from './DataChart';

class App extends Component {

    constructor(props) {
        super(props);

        // Must initialize state first
        this.state = {
            timeMili: [],
            date: "",
            time: "",
            temp: "",
            tempHist: [],
            humi: "",
            humiHist: [],
            presure: "",
            presureHist: []
        };
        this.updateData = () => {
            axios({
                method: 'get',
                url: '/currentdata',
                responseType: 'string'
            }).then(
                (response) => {
                    // handle success
                    const d = this.loadNewData(response.data);
                    this.setState({
                        date: d[1],
                        time: d[2],
                        temp: d[3],
                        humi: d[4],
                        presure: d[5],
                        }
                    );
                });
            setTimeout(this.updateData, 1000);
        };

        this.loadHistData = () => {
            axios({
                method: 'get',
                url: '/data',
                responseType: Array
            }).then(
                (response) => {
                    const time = [];
                    const temp = [];
                    const humi = [];
                    const pressure = [];
                    // handle success
                    response.data.forEach((entry) =>{
                        const d = this.loadNewData(entry);
                        time.push(d[0]);
                        temp.push(Number(d[3]));
                        humi.push(Number(d[4]));
                        pressure.push(Number(d[5]));
                    });
                    this.setState({
                        timeMili: time,
                        tempHist: temp,
                        humiHist: humi,
                        presureHist: pressure
                    });
                });
            setTimeout(this.loadHistData, 600000)
        }

    }

    loadNewData(data) {
        let list = data.split(",");
        const dateMilisec = new Date(Number(list[0]));
        const date = (dateMilisec.getDate().toString().length === 1 ? "0"  + dateMilisec.getDate() : dateMilisec.getDate().toString())
            + "/" +
            ((dateMilisec.getMonth()+1).toString().length === 1 ? "0"  + (dateMilisec.getMonth()+1) : (dateMilisec.getMonth()+1).toString())
            + "/" +
            dateMilisec.getFullYear();
        const time = (dateMilisec.getHours().toString().length === 1 ? "0"  + dateMilisec.getHours() : dateMilisec.getHours().toString())
            + ":" +
            (dateMilisec.getMinutes().toString().length === 1 ? "0"  + dateMilisec.getMinutes() : dateMilisec.getMinutes().toString())
            + ":" +
            (dateMilisec.getSeconds().toString().length === 1 ? "0"  + dateMilisec.getSeconds() : dateMilisec.getSeconds().toString());
        const temp = list[1];
        const humi = list[2];
        const pressure = list[3];
        return [dateMilisec.getTime(),date, time, temp, humi, pressure];
    }

    componentDidMount() {
        // load data.txt
        setTimeout(this.loadHistData);
        setTimeout(this.updateData);
    }

    render() {
        return (
            <div id="outer">
                <p align="center">Datum: {this.state.date}</p>
                <p align="center">Tijd: {this.state.time}</p>
                <div id="inner">
                    <DataChart name="Temperatuur" data={this.state.tempHist} times={this.state.timeMili} color='#993333' unitType="°C"/>
                    <p align="center">Huidige Temperatuur: {this.state.temp} °C</p>
                    <DataChart name="Luchtvochtigheid" data={this.state.humiHist} times={this.state.timeMili} color='#003399'  unitType="rH%"/>
                    <p align="center">Huidige Luchtvochtigheid: {this.state.humi} rH%</p>
                    <DataChart name="Druk" data={this.state.presureHist} times={this.state.timeMili} color='#546E7A' unitType="hPa"/>
                    <p align="center">Huidige Druk: {this.state.presure} hPa</p>
                </div>
                datapunten per grafiek: {this.state.timeMili.length}
            </div>

        )
    }
}
export default App;

/*
<Router>
    <div>
        <NavBar />
        <Route name="home" exact path="/" component={HomePage} />
    </div>
</Router>*/
